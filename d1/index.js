// console.log("Hello World");

// Consitional Statements

let numA = -1;
// if statement
if (numA < 0) {
    console.log(`Hello`);
}
console.log(numA<0);

numA = 0;
if (numA < 0) {
    console.log(`Hello again if numA is 0`);
}
console.log(numA<0);

let city = "New York";
if (city === "New York") {
    console.log(`Welcome to ${city}`);
} 

// else if statement
let numH = 1;

if (numH < 0) {
    console.log(`Hello from numH`);
}
else if (numH > 0) {
    console.log(`Hi I'm H!`);
}
 
city = "Tokyo";

if(city === "New York"){
    console.log(`Welcome to New York`);
}
else if (city === "Tokyo") {
    console.log(`Welcome to Tokyo, Japan`);
    
}

numH = 2;
if (numH < 0) {
    console.log(`Hello I'm numH`);
}
else if (numH > 2) {
console.log(`numH > 2`);
}
else if (numH > 3) {
console.log(`numH > 3`);
}
else {
    console.log(`numH from else`);
}

// if, else if and else statement with function
let message = "No Messagae";

function determineTyphoonIntensity(windSpeed){
    if (windSpeed < 0) {
        return `Invalid Argument`;
    }
    else if (windSpeed > 0 && windSpeed <= 30) {
        return `Tropical depression detected!`;
    }
    else if (windSpeed <= 60) {
        return `Tropical depression detected!`;
    }
    else if (windSpeed >= 61 && windSpeed <= 88) {
        return `Tropical Storm detected.`;
    }
    else if (windSpeed >= 89 && windSpeed <= 117) {
        return `Severe Tropical Storm detected.`;
    }
    else{
        return `Typhoon detected.`;
    }
}
message = determineTyphoonIntensity(123);

console.log(message);
console.warn(message);

// Truthy and Falsy
// Truthy values
/*
    true
    {}
    []
    42
    "0"
    "false"
    new Date()
    -42
    12n
    3.14
    -3.14
    Infinity
    -Infinity
/

// Falsy values
/
  false
  0
  -0
  0n
  "", '', ``
  null
  undefined
  NaN
*/

// Conditional (Ternary) Operator
// Single Statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log(`Result of Ternary Operator: ${ternaryResult}`);

// Multiple Statement execution
let name;
function isOfLegalAge(){
    name = `John`;
    return `You are of the legal age!`;
}
function isUnderAge(){
    name = `Jane`;
    return `You are under age limit!`;
}
// parseInt() convert input receive into number
let age = parseInt(prompt(`What is your age?`));
console.log(age);

let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log(`Result of Ternary opertor in function: ${legalAge}, ${name}`)

// Switch Statement
let day = prompt("What day of the week is it today?").toLowerCase();

switch (day) {
    case 'monday':
        console.log(`The color of the day is red!`);
        break;
    case 'tuesday':
        console.log(`The color of the day is orange!`);
        break
    case 'wednesday':
        console.log(`The color of the day is yellow!`);
        break
    case 'thursday':
        console.log(`The color of the day is green!`);
        break
    case 'friday':
        console.log(`The color of the day is blue!`);
        break
    case 'saturday':
        console.log(`The color of the day is indigo!`);
        break
    case 'sunday':
        console.log(`The color of the day is violet!`);
        break
    default:
        console.log(`Please input valid day!`);
        break;
}

// Try-catch-finally Statement
function showIntensityAlert(windSpeed) {
    try{
        alsdfrt(determineTyphoonIntensity(windSpeed));
    }
    catch(error){
        console.warn(error.message);
    }
    alert("Intensity updates will show alert");

}
showIntensityAlert(110)